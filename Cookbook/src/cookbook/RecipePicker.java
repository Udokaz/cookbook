/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author Udoka
 */
public class RecipePicker implements Initializable {
    
    @FXML private Label recipeTypes;
    @FXML private HBox topHBox;
    @FXML private HBox bottomHBox;
    @FXML private Button mainMenu;
    private static String recipesTypesName;
    private int recipesInRow = 5;
    Set paneListTop = new HashSet();
    Set paneListBottom = new HashSet();

    public static String getRecipesTypesName() {
        return recipesTypesName;
    }

    public static void setRecipesTypesName(String recipesType) {
        recipesTypesName = recipesType + "Recipes";
    }
    
    @FXML public void handleMainMenu(ActionEvent event) throws IOException {
        SceneController newScene = new SceneController();
        newScene.setScene("MainScreen.fxml", mainMenu);
    }
    
    @FXML public void handleRecipeDisplay(ActionEvent event) throws IOException {
        SceneController newScene = new SceneController();
        newScene.setScene("RecipeDisplay.fxml", mainMenu);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        recipeTypes.setText(recipesTypesName);
        int topAmount = 0;
        int bottomAmount = 0;
        if(RecipeBook.numberOfType(recipesTypesName) > recipesInRow) {
            topAmount = recipesInRow;
            if(RecipeBook.numberOfType(recipesTypesName) > (recipesInRow + recipesInRow))
                bottomAmount = recipesInRow;
            else
                bottomAmount = RecipeBook.numberOfType(recipesTypesName) - recipesInRow;
        }
        else
            topAmount = RecipeBook.numberOfType(recipesTypesName);
        List<Recipe> beingDisplayed = RecipeBook.getArrayOfRecipes();
        System.out.println("topamount: " + topAmount);
        for(int i = 0; i < topAmount; i++){
            RecipeDisplay newDisplay = new RecipeDisplay();
            String recipeName;
            Random rand = new Random();
            int randomNumber = rand.nextInt(beingDisplayed.size());
            recipeName = beingDisplayed.get(randomNumber).getNameOfRecipe();
            Pane newPane = new Pane();
            Button newButton = new Button();
            newButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override public void handle(ActionEvent e){
                    if(!newDisplay.setRecipe(recipeName))
                        System.out.println("Recipe was not set for display");
                    SceneController newScene = new SceneController();
                    newScene.setScene("RecipeDisplay.fxml", mainMenu);
                }
            });
            //change the check
            if(!paneListTop.contains(recipeName)){//!paneListTop.contains(newPane.getChildren().getText(recipeName))
                System.out.println("Change the check to look at the children");
                newButton.setText(recipeName);
                newPane.getChildren().add(newButton);
                paneListTop.add(newPane);
                topHBox.getChildren().add(newPane);
                i++;
            }
            
        }
        for(int i = 0; i < bottomAmount; i++){
            RecipeDisplay newDisplay = new RecipeDisplay();
            String recipeName;
            Random rand = new Random();
            int randomNumber = rand.nextInt(beingDisplayed.size());
            recipeName = beingDisplayed.get(randomNumber).getNameOfRecipe();
            Pane newPane = new Pane();
            Button newButton = new Button();
            newButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override public void handle(ActionEvent e){
                    if(!newDisplay.setRecipe(recipeName))
                        System.out.println("Recipe was not set for display");
                    SceneController newScene = new SceneController();
                    newScene.setScene("RecipeDisplay.fxml", mainMenu);
                }
            });
            if(!paneListBottom.contains(recipeName)){
                newButton.setText(recipeName);
                newPane.getChildren().add(newButton);
                paneListBottom.add(newPane);
                bottomHBox.getChildren().add(newPane);
                i++;
            }
            
        }
    }
}
