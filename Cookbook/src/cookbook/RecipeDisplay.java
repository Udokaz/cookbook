/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Udoka
 */
public class RecipeDisplay implements Initializable {
    SceneController newScene = new SceneController();
    private static Recipe myRecipe;
    
    @FXML private Label recipeNameDisplay;
    @FXML private Label recipeDirecitionDisplay;
    @FXML private Button mainMenu;
    @FXML private Button editRecipe;
    @FXML private Button removeRecipe;

    
    @FXML private VBox VBoxAmountDisplay;
    @FXML private VBox VBoxMeasureSizeDispaly;
    @FXML private VBox VBoxFoodItemDispaly;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        recipeNameDisplay.setText(myRecipe.getNameOfRecipe());
        recipeDirecitionDisplay.setText(myRecipe.getRecipeDirections());
        
        for(int i = 0; i < myRecipe.getTotalIngredients(); i++){
            Label newAmountLabel = new Label();
            newAmountLabel.setText(myRecipe.getIngredientsFromList(i).getAmount());
            VBoxAmountDisplay.getChildren().add(newAmountLabel);
            
            Label newMeasureLabel = new Label();
            newMeasureLabel.setText(myRecipe.getIngredientsFromList(i).getMeasureItem());
            VBoxMeasureSizeDispaly.getChildren().add(newMeasureLabel);
            
            Label newFoodItemLabel = new Label();
            newFoodItemLabel.setText(myRecipe.getIngredientsFromList(i).getFoodItem());
            VBoxFoodItemDispaly.getChildren().add(newFoodItemLabel);
        }
    }
    
    public boolean setRecipe(String recipeName){
        boolean temp = false;
        //System.out.println("setRecipe name" + myRecipe.getNameOfRecipe());
        if(RecipeBook.contains(recipeName)){
            myRecipe = RecipeBook.getRecipe(recipeName);
            temp = true;
        }
        else
            System.out.println("containes failed in setRecipe");
        return temp;
    }
    
    @FXML public void displayRecipe(String recipeName){
        System.out.println(RecipeBook.getSizeOfList());
        if(!RecipeBook.contains(recipeName)){
            myRecipe = RecipeBook.getRecipe(recipeName);
            
        }
    }
    
    @FXML public void removeRecipe(){
        RemoveRecipe recipeToRemove = new RemoveRecipe();
        recipeToRemove.setRecipe(recipeNameDisplay.getText());
        newScene.setScene("RemoveRecipe.fxml", removeRecipe);
    }
    
    @FXML public void handleMainMenu(ActionEvent event) throws IOException {
        if(!newScene.setScene("MainScreen.fxml", mainMenu)){
            System.out.println("Failed");
        }
    }
    
    @FXML public void handleEditRecipe(ActionEvent event) throws IOException {
        System.out.println("Create a editScene");
        //newScene.setScene("EditRecipe.fxml", editRecipe);
        
    }
    
      
    
}
