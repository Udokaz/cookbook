/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Udoka
 */
public class MainScreen implements Initializable {
    
    @FXML private Button breakfastScene;
    @FXML private Button mealsScene;
    @FXML private Button dessertScene;
    @FXML private Button snacksScene;
    @FXML private Button newRecipeScene;
    @FXML private Button searchRecipes;
    @FXML private Button saveRecipes;
    @FXML private TextField SearchRecipeName;
    static private int initalized = 0;
    
    
    @FXML public void handleChangeSceneButton(ActionEvent event) throws IOException {
        SceneController newScene = new SceneController();
        
        if(event.getSource() == breakfastScene){
            RecipePicker.setRecipesTypesName("Breakfast ");
            newScene.setScene("RecipePicker.fxml", breakfastScene);
        }
        else if(event.getSource() == mealsScene){
            RecipePicker.setRecipesTypesName("Lunch/Dinner ");
            newScene.setScene("RecipePicker.fxml", mealsScene);
        }
        else if(event.getSource() == dessertScene){
            RecipePicker.setRecipesTypesName("Desserts ");
            newScene.setScene("RecipePicker.fxml", dessertScene);
        }
        else if(event.getSource() == snacksScene){
            RecipePicker.setRecipesTypesName("Snacks ");
            newScene.setScene("RecipePicker.fxml", snacksScene);
        }
        else if(event.getSource() == newRecipeScene){
            if(!newScene.setScene("NewRecipe.fxml", newRecipeScene)){
                System.out.println("NewRecipe failed.");
            }
        }
    }
    
    @FXML public void handleSave(ActionEvent event) throws IOException {
        
        System.out.println("Save here");
    }
    
    @FXML public void handleSearch(ActionEvent event) throws IOException {
        SceneController newScene = new SceneController();
        Alert alert = new Alert(AlertType.INFORMATION);
        
        RecipeDisplay newDisplay = new RecipeDisplay();
        if(newDisplay.setRecipe(SearchRecipeName.getText())) {
            newScene.setScene("RecipeDisplay.fxml", searchRecipes);
        }
        else{
            alert.setTitle("No recipe by that name");
            alert.setHeaderText("No Recipe by that name");
            String s = "There was no recipe by that name.";
            alert.setContentText(s);
            alert.show();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(initalized == 0){
            System.out.println("Add here to import from file");
            initalized = 1;
        }
            
    }    
    
}
