/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

/**
 *
 * @author Administrator
 */
public class Ingredient {
    private String foodItem;
    private String amount;
    private String measureItem;

    public Ingredient(String foodItem, String amount, String measureItem) {
        this.foodItem = foodItem;
        this.amount = amount;
        this.measureItem = measureItem;
    }

    public String getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(String foodItem) {
        this.foodItem = foodItem;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMeasureItem() {
        return measureItem;
    }

    public void setMeasureItem(String measureItem) {
        this.measureItem = measureItem;
    }
}
