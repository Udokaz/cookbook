/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Udoka
 */
public class NewRecipe implements Initializable {

    int startNumberOfIngredients = 3;
    //List ingredientList = new ArrayList();
    Recipe newRecipe = new Recipe();
    
    
    @FXML private Button saveRecipe;
    @FXML private Button browse;
    @FXML private Button newIngredient;
    @FXML private Button cancel;
    
    @FXML private VBox VBoxAmount;
    @FXML private VBox VBoxMeasureSize;     
    @FXML private VBox VBoxFoodItem;
    
    @FXML private TextField newRecipeName;
    @FXML private TextArea recipeDirection;
    @FXML private ChoiceBox recipeType;
    @FXML private TextField imageLocation;
    private List<String> errorMessages = new ArrayList<String>();
    
    ///Ingredient input for new recipe
    
    List<TextField> amountList = new ArrayList<TextField>();
    List<TextField> measureList = new ArrayList<TextField>();
    List<TextField> foodItemList = new ArrayList<TextField>();
     
    
    //adds more ingredients fields to the scene
    @FXML private void createNewIngedientInput(ActionEvent event) throws IOException  {
        TextField newAmountField = new TextField();
        amountList.add(newAmountField);
        VBoxAmount.getChildren().add(newAmountField);
        
        TextField newMeasureField = new TextField();
        measureList.add(newMeasureField);
        VBoxMeasureSize.getChildren().add(newMeasureField);
        
        TextField newFoodItemField = new TextField();
        foodItemList.add(newFoodItemField);
        VBoxFoodItem.getChildren().add(newFoodItemField);
        
        //increase the array number count
        newRecipe.addToTotalIngredients();
    }
    
    @FXML private void getImageLocation(ActionEvent event) throws IOException  {
        System.out.println("Add in the location");
    }
    
    
    @FXML private void saveNewRecipe(ActionEvent event) throws IOException {
       SceneController newScene = new SceneController();
       RecipeDisplay newDisplay = new RecipeDisplay();
       Alert alert = new Alert(Alert.AlertType.INFORMATION);
        
        boolean okToSave = false;
        try{
            if(!newRecipeName.getText().equals("")){
                newRecipe.setNameOfRecipe(newRecipeName.getText());
                okToSave = true;
            }
            else{
                errorMessages.add("Recipe must have a name!");
                System.out.println("Recipe must have a name!");
                okToSave = false;
            }
            if(!recipeDirection.getText().equals("")){
                newRecipe.setRecipeDirections(recipeDirection.getText());
                okToSave = true;
            }
            else{
                errorMessages.add("Recipe must have Directions!");
                System.out.println("Recipe must have Directions!");
                okToSave = false;
            }
            if(!recipeType.getValue().toString().equals("")){
                newRecipe.setTypeOfRecipe(recipeType.getValue().toString());
                okToSave = true;
            }
            else{
                errorMessages.add("Recipe must have a type!");
                System.out.println("Recipe must have a type!");
                okToSave = false;
            }
            //adds all the ingredients to the recipe
            for(int i = 0; i < newRecipe.getTotalIngredients(); i++){
                if(amountList.get(i).getText().equals("") && i == 0){
                    errorMessages.add("Must have at lest one ingredient!");
                    System.out.println("Must have one ingredient!");
                    okToSave = false;
                    break;
                }
                else if(!amountList.get(i).getText().equals("")) {
                    //gets the text from the textfield and puts it into the ingredient
                    newRecipe.addIngredientToList(amountList.get(i).getText(),measureList.get(i).getText(),foodItemList.get(i).getText());
                }
            }
            if(okToSave == true){
                newRecipe.setImageLocation(imageLocation.getText());//set the image location

                if(RecipeBook.addRecipe(newRecipe)){//adds the recipe to the recipebook
                    String name = newRecipe.getNameOfRecipe();
                    amountList.clear();
                    measureList.clear();
                    foodItemList.clear();
                    if(!newDisplay.setRecipe(name))
                        System.out.println("Recipe was not set for display");
                    if(!newScene.setScene("RecipeDisplay.fxml", saveRecipe)){
                        System.out.println("RecipeDisplay was not added");
                    }
                }
                else {

                    System.out.println("newRecipe was not added.");
                }
            }
            else{
                 System.out.println("did not add recipe");
            }
        }
        catch(Exception e){
            System.out.println("Did not add recipe");
            
                alert.setTitle("Unable to add Recipe");
                alert.setHeaderText("The recipe was not added to the RecipeBook!");

                for(int i = 0 ; i < errorMessages.size(); i++){

                    String s = errorMessages.get(i);
                    System.out.println("errormessages " + errorMessages.get(i));
                    alert.setContentText(s);

                }
                alert.show();
                System.out.println("oktosave was false");
        }
    }
    
    @FXML private void handleCancel(ActionEvent event) throws IOException {
        SceneController newScene = new SceneController();
        RecipeDisplay newDisplay = new RecipeDisplay();
        
        String name = newRecipe.getNameOfRecipe();
        amountList.clear();
        measureList.clear();
        foodItemList.clear();
        if(!newScene.setScene("MainScreen.fxml", saveRecipe)){
            System.out.println("RecipeDisplay was not added");
        }
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //creates the options for the choicebox
        recipeType.setItems(FXCollections.observableArrayList("Breakfast","Lunch/Dinner","Snacks","Desserts"));
        
        //creates startNumberOfIngredients number of new ingredients to be displayed on the screen
        for(int i = 0; i < startNumberOfIngredients; i++){
            TextField newAmountField = new TextField();
            amountList.add(newAmountField);
            VBoxAmount.getChildren().add(newAmountField);

            TextField newMeasureField = new TextField();
            measureList.add(newMeasureField);
            VBoxMeasureSize.getChildren().add(newMeasureField);

            TextField newFoodItemField = new TextField();
            foodItemList.add(newFoodItemField);
            VBoxFoodItem.getChildren().add(newFoodItemField);
            newRecipe.addToTotalIngredients();
        }
        
    }
    
    
    
}
