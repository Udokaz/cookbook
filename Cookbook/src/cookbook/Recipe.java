/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class Recipe {
    private String nameOfRecipe;
    private List<Ingredient> recipeIngredients = new ArrayList<Ingredient>(); //stores all the ingredients that are used in the recipe
    private String recipeDirections;//stoers the directions of the recipe
    private String imageLocation;//stores the location of the image to use with the recipe
    private int totalIngredients;
    private String typeOfRecipe;
    
    //adds an ingredient to the list
    public void addIngredientToList(String amount, String measure, String foodItem){
        Ingredient newIngredientItem = new Ingredient("1","cup","beef");
        newIngredientItem.setAmount(amount);
        newIngredientItem.setMeasureItem(measure);
        newIngredientItem.setFoodItem(foodItem);
        recipeIngredients.add(newIngredientItem);
    }
    
    //might do all at once or one at a time
    //This is to get the ingredients from the list
    public Ingredient getIngredientsFromList(int location){
        return recipeIngredients.get(location);
    }
    
    //Checks if the ingedient is in the list. If so it removes it and returns true. if not returns false
    public boolean removeIngredientFromList(String ingredientName){
        for(int i = 0; i < recipeIngredients.size(); i++ ){
            if(recipeIngredients.get(i).getFoodItem().equals(ingredientName)){
                recipeIngredients.remove(i);
                return true;
            }                
        } 
        return false;
    }

    public String getNameOfRecipe() {
        return nameOfRecipe;
    }

    public void setNameOfRecipe(String RecipeName) {
        nameOfRecipe = RecipeName;
    }
    
    public int sizeOfList(){
        return recipeIngredients.size();
    }

    public String getRecipeDirections() {
        return recipeDirections;
    }

    public void setRecipeDirections(String directionsRecipe) {
        recipeDirections = directionsRecipe;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String locationOfImage) {
        imageLocation = locationOfImage;
    }
    
    public void addToTotalIngredients(){
        totalIngredients++;
    }
    
    public int getTotalIngredients(){
        return totalIngredients;
    }

    public String getTypeOfRecipe() {
        return typeOfRecipe;
    }

    public void setTypeOfRecipe(String typeOfRecipe) {
        this.typeOfRecipe = typeOfRecipe;
    }
    
    
}
