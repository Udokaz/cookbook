/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author Udoka
 */
public class RemoveRecipe implements Initializable {
    private static Recipe myRecipe;
    
    @FXML private Label recipeNameDisplay;
    @FXML private Label recipeDirecitionDisplay;
    @FXML private Button mainScreen;
    @FXML private Button removeRecipe;

    
    @FXML private VBox VBoxAmountDisplay;
    @FXML private VBox VBoxMeasureSizeDispaly;
    @FXML private VBox VBoxFoodItemDispaly;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        recipeNameDisplay.setText(myRecipe.getNameOfRecipe());
        recipeDirecitionDisplay.setText(myRecipe.getRecipeDirections());
        
        for(int i = 0; i < myRecipe.getTotalIngredients(); i++){
            Label newAmountLabel = new Label();
            newAmountLabel.setText(myRecipe.getIngredientsFromList(i).getAmount());
            VBoxAmountDisplay.getChildren().add(newAmountLabel);
            
            Label newMeasureLabel = new Label();
            newMeasureLabel.setText(myRecipe.getIngredientsFromList(i).getMeasureItem());
            VBoxMeasureSizeDispaly.getChildren().add(newMeasureLabel);
            
            Label newFoodItemLabel = new Label();
            newFoodItemLabel.setText(myRecipe.getIngredientsFromList(i).getFoodItem());
            VBoxFoodItemDispaly.getChildren().add(newFoodItemLabel);
        }
    }
    
    public boolean setRecipe(String recipeName){
        boolean temp = false;
        //System.out.println("setRecipe name" + myRecipe.getNameOfRecipe());
        if(RecipeBook.contains(recipeName)){
            myRecipe = RecipeBook.getRecipe(recipeName);
            temp = true;
        }
        else
            System.out.println("containes failed in setRecipe");
        return temp;
    }
    
    @FXML public void displayRecipe(String recipeName){
        System.out.println(RecipeBook.getSizeOfList());
        if(!RecipeBook.contains(recipeName)){
            myRecipe = RecipeBook.getRecipe(recipeName);
            
        }
    }
    
    @FXML public void removeRecipe(){
        System.out.println("recipeNameDisplay.getText() = " + recipeNameDisplay.getText());
        if(RecipeBook.contains(recipeNameDisplay.getText())){
            
            SceneController newScene = new SceneController();
            System.out.println("main scene");
            if(!newScene.setScene("MainScreen.fxml", removeRecipe)){
                System.out.println("Failed!");
            }
            System.out.println("removing recipe now");
            RecipeBook.removeRecipe(recipeNameDisplay.getText());
            if(RecipeBook.contains(recipeNameDisplay.getText())){
                System.out.println("Recipe is still there");
            }
            else
                System.out.println("recipe is gone");
        }
    }
    
    @FXML public void handleMainMenu(ActionEvent event) throws IOException {
        SceneController newScene = new SceneController();
        if(newScene.setScene("MainScreen.fxml", mainScreen))
            System.out.println("mainscreen");
    }
}
