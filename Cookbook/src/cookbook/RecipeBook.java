/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cookbook;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;



/**
 *
 * @author Udoka
 *This is a signelton class. There should only be one Recipebook at a time. 
 */
public class RecipeBook {
    
    private final static RecipeBook instance = new RecipeBook();
    
    private RecipeBook(){}
    
    private static int sizeOfList;
    private static int numberOfBreakfast;
    private static int numberOfMeal;
    private static int numberOfSnack;
    private static int numberOfDessert;

    
    public static RecipeBook getInstance() {
        return instance;
    }
    
    private static Set<Recipe> myRecipes = new HashSet<Recipe>();
    
    public static boolean addRecipe( Recipe newRecipe){
        boolean temp = false;
        if(myRecipes.contains(newRecipe.getNameOfRecipe()))
            return false;
        if(myRecipes.add(newRecipe)){
            sizeOfList++;
            if(newRecipe.getTypeOfRecipe() == "Breakfast")
                numberOfBreakfast++;
            if(newRecipe.getTypeOfRecipe() == "Lunch/Dinner")
                numberOfMeal++;
            if(newRecipe.getTypeOfRecipe() == "Snacks")
                numberOfSnack++;
            if(newRecipe.getTypeOfRecipe() == "Desserts")
                numberOfDessert++;
            temp = true;
        }
        else {
            System.out.println("Recipe did not add");
            temp = false;
        }
        return temp;
    }
    
    public static boolean removeRecipe( String removeRecipe ){
        boolean temp = false;
        for(Recipe look: myRecipes){
            if(look.getNameOfRecipe().contains(removeRecipe)){
                if(look.getTypeOfRecipe() == "Breakfast")
                    numberOfBreakfast--;
                if(look.getTypeOfRecipe() == "Lunch/Dinner")
                    numberOfMeal--;
                if(look.getTypeOfRecipe() == "Snacks")
                    numberOfSnack--;
                if(look.getTypeOfRecipe() == "Desserts")
                    numberOfDessert--;
            
                myRecipes.remove(look);
                System.out.println("recipe removed");
                temp = true;
            }
        }
        return temp;
    }
    
    public static ArrayList getArrayOfRecipes(){
        ArrayList arrayOfRecipes = new ArrayList();
        Iterator<Recipe> ita = myRecipes.iterator();
        for(int i = 0; i < myRecipes.size(); i++){
            arrayOfRecipes.add(i, ita.next());
        }
        return arrayOfRecipes;
    }
    
    public static int numberOfType(String typeToGet){
        int total = 0;
        if(typeToGet.equals("Breakfast Recipes"))
            total = numberOfBreakfast;
        else if(typeToGet.equals("Lunch/Dinner Recipes"))
            total = numberOfMeal;
        else if(typeToGet.equals("Snacks Recipes"))
            total = numberOfSnack;
        else if(typeToGet.equals("Desserts Recipes"))
            total = numberOfDessert;
        System.out.println("Breakfast: " + numberOfBreakfast + "Lunch/Dinner " + numberOfMeal + "Snacks "+ numberOfSnack + "Desserts " + numberOfDessert);
        return total;
    }
    
    public static boolean contains( String recipeName ){
        boolean temp = false;
        for(Recipe look: myRecipes){
            System.out.println(look.getNameOfRecipe().compareTo(recipeName));
            if(look.getNameOfRecipe().compareTo(recipeName) == 0){
                temp = true;
                break;
            }
            else 
                temp = false;
        }
        return temp;
    }
    
    public static int getRecipeLocation( String recipeName ){
        int count = 0;
        for(Recipe look: myRecipes){
            if(look.getNameOfRecipe() != recipeName)
                count++;
        }
        return count;
    }
    
    public static Recipe getRecipe( String recipeName ){
        for(Recipe look: myRecipes){
            if(look.getNameOfRecipe().contains(recipeName)){
                return look;
            }
        }
        System.out.println("Recipe not there");
        return null;
    }
    
    public static Recipe getRandomRecipe( ){
        Iterator<Recipe> ita = myRecipes.iterator();
        return ita.next();
    }
    
    public static int getSizeOfList() {
        return sizeOfList;
    }

    public static void setSizeOfList() {
        sizeOfList++;
    }
}
